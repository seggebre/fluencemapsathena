#!/bin/sh
echo "Setting up all the things..."

setupATLAS --quiet
lsetup cmake

# Figure out which version of Linux OS is being used from OS CPE name
cpe_name=`cat /etc/os-release | grep CPE_NAME`
cpe_name=${cpe_name:9}

if  [[ $cpe_name == *"centos:7"* ]]; then
    lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
fi

