
#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <dirent.h>
#include <string>
#include <vector>

#include "RadDam/HelperFunctions.h"

namespace RadDam_Helper{

    std::vector<std::string> GetDirectoryList(std::string InputPath){

        DIR *Directory;
        struct dirent *diread;
        std::vector<std::string> OutputList;

        if((Directory = opendir(InputPath.c_str())) != nullptr){
            while((diread = readdir(Directory)) != nullptr){
                OutputList.push_back(diread->d_name);
            }
            closedir(Directory);
        }
        else{
            std::cout<< "ERROR: Input Directory for " << InputPath << " not found." <<std::endl;
            exit(EXIT_FAILURE);
        }

        OutputList.erase(remove(OutputList.begin(), OutputList.end(), "."), OutputList.end());
        OutputList.erase(remove(OutputList.begin(), OutputList.end(), ".."), OutputList.end());

        delete diread;
        return OutputList;
    }

    std::string removeAll(std::string str, const std::string& from){
        size_t start_pos = 0;
        
        while( ( start_pos = str.find( from)) != std::string::npos){
            str.erase( start_pos, from.length());
        }
        
        return str;
    }

    TVectorD CastStdVec(const std::vector<double> Input){
        TVectorD TempArray(Input.size());
        for(uint iIndex = 0; iIndex<Input.size(); iIndex++){
           TempArray[iIndex] = Input.at(iIndex);
        }
        return TempArray;
    }

    double FindClosestElement(std::vector<double> Vector, double Value, bool Larger){

        //std::remove(Vector.begin(), Vector.end(), Value);

        if(Larger == true){
            std::vector<double> LargerValues;
            for(uint iEntry=0; iEntry<Vector.size(); iEntry++){
                if(Vector.at(iEntry)>=Value) LargerValues.push_back(Vector.at(iEntry));
            }

            if(LargerValues.size()==0) return Value;
            return *min_element(LargerValues.begin(), LargerValues.end());
        }

        else{
            std::vector<double> SmallerValues;
            for(uint iEntry=0; iEntry<Vector.size(); iEntry++){
                if(Vector.at(iEntry)<=Value) SmallerValues.push_back(Vector.at(iEntry));
            }
            if(SmallerValues.size()==0) return Value;
            return *max_element(SmallerValues.begin(), SmallerValues.end());
        }
    }
}