#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <dirent.h>

#include "RadDam/InputOptions.h"

namespace RadDam_InputOptions{

    void PrintUsage(){
        std::cout << "You need to pass the Name of the InputFile and the Output Directory." <<std::endl;
        std::cout << "Optional, a third argument can be provided, which specifies a Directory, where Comparison Plots are stored" <<std::endl;
        std::cout << "The Comparison Plots uses the closest Fluence and Voltage Values, which are not present in the TCAD List, and plot them together." <<std::endl;
        std::cout << "The InputFile should contain the following Input Features, seperated by Comma (there should be no whitespace between the Values)" <<std::endl;
        std::cout << "1) Sensor Thickness in µm." <<std::endl;
        std::cout << "2) Voltage in V." <<std::endl;
        std::cout << "3) Fluence in e14 neq/cm^2." <<std::endl;
        std::cout << "4) Temperature in Celcius." <<std::endl;
        std::cout << "For example: '250,300,3.5,12' would be a 250µm Sensor with a Bias Voltage of 300V radiated by 3.5e14 neq/cm^2 at 12 Celcius" <<std::endl;
    }

    std::vector<std::string> GetFileContent(std::string FileName){

        std::vector<std::string> Content;
        std::string item_name;
        std::ifstream file_name;
        file_name.open(FileName.c_str());
        std::string line;

        while(getline(file_name, line)){
            Content.push_back(line);
        }

        return Content;
    }

    std::vector<std::string> GetDirectoryList(std::string InputPath){

        DIR *Directory;
        struct dirent *diread;
        std::vector<std::string> OutputList;

        if((Directory = opendir(InputPath.c_str())) != nullptr){
            while((diread = readdir(Directory)) != nullptr){
                OutputList.push_back(diread->d_name);
            }
            closedir(Directory);
        }
        else{
            std::cout<< "ERROR: Input Directory for " << InputPath << " not found." <<std::endl;
            exit(EXIT_FAILURE);
        }

        OutputList.erase(remove(OutputList.begin(), OutputList.end(), "."), OutputList.end());
        OutputList.erase(remove(OutputList.begin(), OutputList.end(), ".."), OutputList.end());

        for(unsigned int iEntry=0; iEntry<OutputList.size(); iEntry++){
            OutputList.at(iEntry) = InputPath + OutputList.at(iEntry);
        }

        delete diread;
        return OutputList;
    }

    std::vector<std::string> SplitInput(std::string& s, char delimiter){                                                                                                                                                                                             
        std::vector<std::string> splits;                                                                                                                                                           
        std::string split;                                                                                                                                                                         
        std::istringstream ss(s);                                                                                                                                                                  
        
        while (std::getline(ss, split, delimiter)){                                                                                                                                                                                          
            splits.push_back(split);                                                                                                                                                                
        }                                                                                                                                                                                          
        return splits;                                                                                                                                                                             
    }

    ConfigOptions get_config_opts(int argc, char* argv[]){

        if(argc != 3 and argc != 4){
            PrintUsage();
            exit(EXIT_FAILURE);
        }

        std::vector<double> SensorThicknesses;
        std::vector<double> Voltages;
        std::vector<double> Fluences;
        std::vector<double> Temperatures;

        auto InputLines = GetFileContent(argv[1]);
        std::string OutputDirectory = argv[2];
        std::string ComparisonDirectory = "";
        if(argc == 4) ComparisonDirectory = argv[3];

        for(uint iLine=0; iLine<InputLines.size(); iLine++){
            auto Values = SplitInput(InputLines.at(iLine), ',');
            SensorThicknesses.push_back(std::stod(Values.at(0)));
            Voltages.push_back(std::stod(Values.at(1)));
            Fluences.push_back(std::stod(Values.at(2)));
            Temperatures.push_back(std::stod(Values.at(3)));
        }

        if(SensorThicknesses.size() != Voltages.size() or Fluences.size() != Temperatures.size()){
            std::cout << "Number of Input Sensor Thicknesses, Voltages, Fluences, and Temperatures do not match." << std::endl;
            exit(EXIT_FAILURE);
        }

        ConfigOptions opts;
        opts.SensorThicknesses = SensorThicknesses;
        opts.Voltages = Voltages;
        opts.Fluences = Fluences;
        opts.Temperatures = Temperatures;
        opts.OutputDirectory = OutputDirectory;
        opts.ComparisonDirectory = ComparisonDirectory;

        return opts;
    }

}