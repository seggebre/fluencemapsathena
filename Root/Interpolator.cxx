#include "RadDam/Interpolator.h"
#include "RadDam/HelperFunctions.h"
#include "RadDam/MapComparison.h"

#include "TROOT.h"
#include "TH1.h"
#include "TF1.h"
#include "TVectorD.h"
#include <TFile.h>
#include <TMath.h>
#include <TGraph.h>
#include <algorithm>
#include <stdexcept>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

Interpolator::Interpolator(double aimFluence, double aimVoltage, int SensorDepth, std::string ComparisonDirectory){
  m_aimFluence = aimFluence;
  m_aimVoltage = aimVoltage;
  m_SensorDepth = SensorDepth;
  m_ComparisonDirectory = ComparisonDirectory;

  m_TextFluence = "#Phi=" + TString::Format("%.2f",m_aimFluence);
  m_TextVoltage = "U="    + TString::Format("%.0f",m_aimVoltage);
  m_TextSensorDepth = "z[#mum]=" + std::to_string(m_SensorDepth);
}

Interpolator::~Interpolator(){
  delete m_InputFile;

  for(uint iContainer=0; iContainer<m_EFieldProfiles.size(); iContainer++){
    delete m_EFieldProfiles.at(iContainer);
  }
}

TH1F* Interpolator::GetEField(){

  auto EFieldProfile = CreateEFieldProfile();

  if(!EFieldProfile){
    std::cerr <<"ERROR: Interpolator::GetEField: EField is Nullptr." <<std::endl;
    exit(EXIT_FAILURE);
  }

  return EFieldProfile;
}

TH1F* Interpolator::CreateEFieldProfile(){

  m_EFieldProfile = new TH1F("EField1D", (m_TextFluence + ", " + m_TextVoltage + ", " + m_TextSensorDepth).c_str(), m_SensorDepth, -0.5, m_SensorDepth-0.5);

  std::cout << "INFO\t" << "Interpolator::CreateEFieldProfile: Inisilaise Files." << std::endl;

  InitaliseFiles();

  std::cout << "INFO\t" << "Interpolator::CreateEFieldProfile: Estimate Field." << std::endl;

  int NegativeCounter = 0;

  for(int iSensorPart=0; iSensorPart<m_SensorDepth; iSensorPart++){
    if(iSensorPart<=3 or iSensorPart>=m_SensorDepth-3) continue;
    auto ExtraplatedEFieldValue = EstimateEField(iSensorPart, "Spline");
    if(ExtraplatedEFieldValue<0) NegativeCounter++;
    m_EFieldProfile->SetBinContent(iSensorPart+1, ExtraplatedEFieldValue);
  }

  if(NegativeCounter>0){
    std::cout << "WARNING\t" << "Interpolator::CreateEFieldProfile: Unphysical Field: Estimate Field linearly." << std::endl;
    for(int iSensorPart=0; iSensorPart<m_SensorDepth; iSensorPart++){
      if(iSensorPart<=3 or iSensorPart>=m_SensorDepth-3) continue;
      auto ExtraplatedEFieldValue = EstimateEField(iSensorPart, "Linear");
      m_EFieldProfile->SetBinContent(iSensorPart+1, ExtraplatedEFieldValue);
      m_EFieldProfile->SetTitle((m_TextFluence + ", " + m_TextVoltage + ", " + m_TextSensorDepth + ", linear").c_str());
    }    
  }

  std::cout << "INFO\t" << "Interpolator::CreateEFieldProfile: Fill Edges." << std::endl;
  //Fill Edges of the EField Profile.
  ExtrapolateEdges();
  //Scale EField
  ScaleEField();

  //Check if Fluence is present in TCAD List
  bool IsFluenceInList = true;
  if(std::find(m_PossibleFluenceValues.begin(), m_PossibleFluenceValues.end(), m_aimFluence) == m_PossibleFluenceValues.end()) IsFluenceInList = false;

  if(m_ComparisonDirectory != "" and IsFluenceInList == false){
    std::cout << "INFO\t" << "Interpolator::CreateEFieldProfile: Comparison Plot will be created." << std::endl;
    TH1F* EField = new TH1F();
    EField = (TH1F*)m_EFieldProfile->Clone();
    MapComparison* MP = new MapComparison(EField, m_aimFluence, m_aimVoltage, m_SensorDepth, m_ComparisonDirectory);
    MP->CreateOnFlyComparison();
    delete MP;
  }

  return m_EFieldProfile;
}

void Interpolator::InitaliseFiles(){

  using namespace RadDam_Helper;

  if(m_SensorDepth>200 && m_SensorDepth<=250){
    m_SensorDepth = 250;
    m_TCAD_List = "TCAD_Blayer_efields/fei4-250um/";
  }

  if(m_SensorDepth<=200){
    m_SensorDepth = 200;
    m_TCAD_List = "TCAD_IBL_efields/fei4-200um/";
  }

  if(m_SensorDepth>250){
    std::cerr <<"ERROR: Interpolator::InitaliseFiles: Sensor Depth has to be smaller than 250 [µm]." <<std::endl;
    exit(EXIT_FAILURE);    
  }

  auto FluenceDirectories = GetDirectoryList(m_TCAD_List.c_str());

  //Double Loop over all Fl* Directories and E_Field*V.dat Files to get the values and the corresponding voltage and fluence values.
  //Save all information in the EFieldProfiles which are structs EFieldContainers having all information about the profile.

  for(uint iDirectory = 0; iDirectory<FluenceDirectories.size(); iDirectory++){

    //Get Fluence Value from the Directory List:
    std::string Fluence_String = FluenceDirectories.at(iDirectory);
    Fluence_String = removeAll(Fluence_String, "fl");
    double FluenceValue = 0;
    try{
      FluenceValue = std::stod(Fluence_String);
    }
    catch(const std::invalid_argument&){
      //If Argument of string to double conversion is invalid, just skip Directory.
      continue;
    }
    
    //If Fluence Value is not in the Vector, add it:
    if(std::find(m_PossibleFluenceValues.begin(), m_PossibleFluenceValues.end(), FluenceValue) == m_PossibleFluenceValues.end()){
      m_PossibleFluenceValues.push_back(FluenceValue*1e-14);
    }

    auto VoltageFiles = GetDirectoryList((m_TCAD_List + FluenceDirectories.at(iDirectory) + "/reference/").c_str());

    for(uint iFile = 0; iFile<VoltageFiles.size(); iFile++){
      //Get also the Voltage Value

      std::string Voltage_String = VoltageFiles.at(iFile);
      Voltage_String = removeAll(Voltage_String, "E_Field-");
      Voltage_String = removeAll(Voltage_String, "V.dat");

      double VoltageValue = 0;

      try{
        VoltageValue = std::stod(Voltage_String);
      }
      catch(const std::invalid_argument&){
        //If Argument of string to double conversion is invalid, just skip File.
        continue;
      }

      //If Voltage Value is not in the Vector, add it:
      if(std::find(m_PossibleVoltageValues.begin(), m_PossibleVoltageValues.end(), VoltageValue) == m_PossibleVoltageValues.end()){
        m_PossibleVoltageValues.push_back(VoltageValue);
      }

      std::string InputFilePath = m_TCAD_List  + FluenceDirectories.at(iDirectory) + "/reference/" + VoltageFiles.at(iFile);

      //Function to Fill the Containers and collect them as a vector of pointers which is called EFieldProfiles.
      FillEFieldContainer(InputFilePath.c_str(), FluenceValue, VoltageValue);
    }
  }
}

void Interpolator::FillEFieldContainer(std::string InputFile, double FluenceValue, double VoltageValue){

  EFieldContainer* EC = new EFieldContainer;
  TH1F* EField = new TH1F("", "", m_SensorDepth, -0.5, m_SensorDepth-0.5);

  EC->FluenceValue = FluenceValue*1e-14; //Fluence is in e14 1 MeV neq/cm^2
  EC->VoltageValue = VoltageValue;  // Voltage is in V
  EC->SensorSize = m_SensorDepth;   // Sensor Depth is in µm

  std::ifstream File;
  File.open(InputFile.c_str());

  if(!File){
    std::cout << "ERROR: Interpolator::FillEFieldContainer:: Could not open File: " << InputFile <<std::endl;
    exit(EXIT_FAILURE);
  }

  double SensorPosition = 0;
  double EFieldValue = 0;

  while(File >> SensorPosition >> EFieldValue){
    EField->Fill(SensorPosition, EFieldValue);
  }

  File.close();

  //Cut Edges of the EField Input Profile. Remove first four and last three bins:
  for(int iBin=0; iBin<EField->GetNbinsX(); iBin++){
    if(iBin <= 3 or iBin >=EField->GetNbinsX()-3) EField->SetBinContent(iBin+1, 0);
  }

  EC->EField = EField;
  m_EFieldProfiles.push_back(EC);
}

double Interpolator::EstimateEField(int SensorPart, std::string InterpolationType){

  if(InterpolationType != "Linear" and InterpolationType != "Spline"){
    std::cout << "ERROR: Interpolator::EstimateEField: Invalid Interpolation Argument, select 'Spline' or 'Linear'." <<std::endl;
    exit(EXIT_FAILURE);
  }

  using namespace RadDam_Helper;

  std::vector<double> Available_Voltage;
  std::vector<double> Extrapolated_EField;

  for(uint iVoltage=0; iVoltage<m_PossibleVoltageValues.size(); iVoltage++){

    std::vector<double> Available_EField;
    std::vector<double> Available_Fluence;

    for(uint iFluence=0; iFluence<m_PossibleFluenceValues.size(); iFluence++){
      for(uint iContainer=0; iContainer<m_EFieldProfiles.size(); iContainer++){
        if(m_EFieldProfiles.at(iContainer)->VoltageValue==m_PossibleVoltageValues.at(iVoltage) and m_EFieldProfiles.at(iContainer)->FluenceValue==m_PossibleFluenceValues.at(iFluence)){
          Available_Fluence.push_back(m_PossibleFluenceValues.at(iFluence));
          Available_EField.push_back(m_EFieldProfiles.at(iContainer)->EField->GetBinContent(SensorPart+1));
        }
        else{
          continue;
        }
      }
    }

    TGraph* TempGraph = new TGraph(CastStdVec(Available_Fluence), CastStdVec(Available_EField));
    auto Extrapolated_EField_Value = 0;
    if(InterpolationType == "Spline") Extrapolated_EField_Value = TempGraph->Eval(m_aimFluence, 0, "S");
    if(InterpolationType == "Linear") Extrapolated_EField_Value = TempGraph->Eval(m_aimFluence);
    Extrapolated_EField.push_back(Extrapolated_EField_Value);
    Available_Voltage.push_back(m_PossibleVoltageValues.at(iVoltage));

    delete TempGraph;
  }

  TGraph* TempGraph = new TGraph(CastStdVec(Available_Voltage), CastStdVec(Extrapolated_EField));  
  auto aimEField = 0;
  if(InterpolationType == "Spline") aimEField = TempGraph->Eval(m_aimVoltage, 0, "S");
  if(InterpolationType == "Linear") aimEField = TempGraph->Eval(m_aimVoltage);
  
  delete TempGraph;
  return aimEField;
}

void Interpolator::ExtrapolateEdges(){

  using namespace RadDam_Helper;

  //Get Content of Histo and generate TGraph to extrapolate the edges.
  std::vector<double> SensorDepth;
  std::vector<double> EField;

  for(int iSensorPart=0; iSensorPart<m_SensorDepth; iSensorPart++){
    if(iSensorPart<=3 or iSensorPart>=m_SensorDepth-3) continue;

    SensorDepth.push_back((double)iSensorPart);
    EField.push_back(m_EFieldProfile->GetBinContent(iSensorPart+1));
  }

  TGraph* TempGraph = new TGraph(CastStdVec(SensorDepth), CastStdVec(EField));  

  for(int iSensorPart=0; iSensorPart<m_SensorDepth; iSensorPart++){
    if(iSensorPart<=3 or (iSensorPart>=m_SensorDepth-3)){
      auto ExtrapolatedEFieldValue = TempGraph->Eval((double)iSensorPart, 0, "S");
      m_EFieldProfile->SetBinContent(iSensorPart+1, ExtrapolatedEFieldValue);
    }
  }

  delete TempGraph;
}

void Interpolator::ScaleEField(){
  m_EFieldProfile->Scale((m_aimVoltage*10000)/(float)m_EFieldProfile->Integral());
}