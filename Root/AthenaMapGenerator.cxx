#include "TFile.h"
#include "TCanvas.h"
#include <iostream>
#include "TH3F.h"
#include "TProfile.h"
#include "TLegend.h"
#include "TH2F.h"
#include "TH1F.h"  
#include "TStyle.h"
#include "TString.h"
#include <sstream> 
#include <string>  
#include <cstring>  
#include <fstream> 
#include "TTree.h"
#include "TRandom3.h"
#include <boost/algorithm/string/replace.hpp>

#include "RadDam/HelperFunctions.h"
#include "RadDam/AthenaMapGenerator.h"
#include "RadDam/Interpolator.h"

AthenaMapGenerator::AthenaMapGenerator(double SensorThickness, double BiasVoltage, double Fluence, double Temperature, std::string OutputDirectory, std::string ComparisonDirectory){
    m_SensorThickness_mm = SensorThickness/1000;    //in mm
    m_SensorThickness_mum = SensorThickness;        //in µm    
    m_BiasVoltage = BiasVoltage;                    //in V
    m_Fluence = Fluence;                            //in e14 neq/cm^2
    m_Temperature = Temperature;
    m_OutputDirectory = OutputDirectory;
    m_ComparisonDirectory = ComparisonDirectory;

    WelcomeMessage();

    m_DistanceMap_e = new TH2F("edistance","Electron Distance Map", 100, 0, m_SensorThickness_mm, 1000, 0, 100);
    m_DistanceMap_h = new TH2F("hdistance","Holes Distance Map", 100, 0, m_SensorThickness_mm, 1000, 0, 100);
    m_TimeMap_e = new TH1F("etimes", "Electron Time Map", 100, 0, m_SensorThickness_mm);
    m_TimeMap_h = new TH1F("htimes", "Hole Time Map" ,100 ,0 , m_SensorThickness_mm);
    m_LorentzMap_e = new TH2F("lorentz_map_e","Lorentz Map e", 100, 0, m_SensorThickness_mm, 100, 0, m_SensorThickness_mm);
    m_LorentzMap_h = new TH2F("lorentz_map_h","Lorentz Map h", 100, 0, m_SensorThickness_mm, 100, 0, m_SensorThickness_mm);

    m_AverageLorentzAngle_e = new TProfile("averageLorentzAngle_e","Average Lorentz angle for electrons vs initial position;z [mm];<tan(#theta_{LA})>", 100, 0, m_SensorThickness_mm);
    m_AverageLorentzAngle_h = new TProfile("averageLorentzAngle_h","Average Lorentz angle for holes vs initial position;z [mm];<tan(#theta_{LA})>", 100, 0, m_SensorThickness_mm);
}

AthenaMapGenerator::~AthenaMapGenerator(){
    delete m_DistanceMap_e;
    delete m_DistanceMap_h;
    delete m_TimeMap_e;
    delete m_TimeMap_h;
    delete m_LorentzMap_e;
    delete m_LorentzMap_h;

    delete m_AverageLorentzAngle_e;
    delete m_AverageLorentzAngle_h;
}

void AthenaMapGenerator::WelcomeMessage(){

    std::cout << "                                                             " <<std::endl;
    std::cout << "#############################################################" <<std::endl;
    std::cout << "#           Welcome to the Athena-Map Interpolator          #" <<std::endl;
    std::cout << "#############################################################" <<std::endl;
    std::cout << "                                                             " <<std::endl;
    std::cout << "  Map is generated or the following input parameter:         " <<std::endl;
    std::cout << "  Seonsor Size [µm]: " << m_SensorThickness_mum << std::endl;
    std::cout << "  Fluence in [e14 neq/cm^2]: " << m_Fluence<<std::endl;
    std::cout << "  Voltage in [V]: " << m_BiasVoltage <<std::endl;
    std::cout << "  Temperature [C]: " << m_Temperature <<std::endl;
    std::cout << "  Maps are stored to: " << m_OutputDirectory <<std::endl;
    if(m_ComparisonDirectory != "") std::cout << "  Comparison Plots are created and stored to: " << m_ComparisonDirectory <<std::endl;
    std::cout << "                                                              " <<std::endl;
}

void AthenaMapGenerator::DecorateHistograms(TH1F* EFieldMap){

	m_DistanceMap_e->GetXaxis()->SetNdivisions(505);
	m_DistanceMap_e->GetYaxis()->SetNdivisions(505);
	m_DistanceMap_e->GetXaxis()->SetTitle("Initial Position in Z [mm]");
	m_DistanceMap_e->GetYaxis()->SetTitle("Time Traveled [ns]");
	m_DistanceMap_e->GetZaxis()->SetTitleOffset(1.6);
	m_DistanceMap_e->SetTitle("Electron Distance Map");
	m_DistanceMap_e->GetZaxis()->SetTitle("Location in Z [mm]");  

    m_DistanceMap_h->GetXaxis()->SetNdivisions(505);
    m_DistanceMap_h->GetYaxis()->SetNdivisions(505);
    m_DistanceMap_h->GetXaxis()->SetTitle("Initial Position in Z [mm]");
    m_DistanceMap_h->GetYaxis()->SetTitle("Time Traveled [ns]");
    m_DistanceMap_h->GetZaxis()->SetTitleOffset(1.6);
    m_DistanceMap_h->SetTitle("Hole Distance Map");
    m_DistanceMap_h->GetZaxis()->SetTitle("Location in Z [mm]");

	m_TimeMap_h->SetTitle("");
	m_TimeMap_h->GetYaxis()->SetRangeUser(0,m_TimeMap_h->GetMaximum() > m_TimeMap_e->GetMaximum() ? m_TimeMap_h->GetMaximum() : m_TimeMap_e->GetMaximum());
	m_TimeMap_h->GetXaxis()->SetTitle("Starting Pixel Depth in Z [mm]");
	m_TimeMap_h->GetYaxis()->SetTitleOffset(1.4);
	m_TimeMap_h->GetYaxis()->SetTitle("Projected Time to Reach Electrode [ns]");
	m_TimeMap_h->GetXaxis()->SetNdivisions(505);

	m_TimeMap_e->SetTitle("");
	m_TimeMap_e->GetYaxis()->SetRangeUser(0,m_TimeMap_h->GetMaximum() > m_TimeMap_e->GetMaximum() ? m_TimeMap_h->GetMaximum() : m_TimeMap_e->GetMaximum());
	m_TimeMap_e->GetXaxis()->SetTitle("Starting Pixel Depth in Z [mm]");
	m_TimeMap_e->GetYaxis()->SetTitleOffset(1.4);
	m_TimeMap_e->GetYaxis()->SetTitle("Projected Time to Reach Electrode [ns]");
	m_TimeMap_e->GetXaxis()->SetNdivisions(505);

	m_LorentzMap_e->GetXaxis()->SetNdivisions(505);
	m_LorentzMap_e->GetYaxis()->SetNdivisions(505);
	m_LorentzMap_e->GetXaxis()->SetTitle("Initial Position in Z [mm]");
	m_LorentzMap_e->GetYaxis()->SetTitle("Distance Traveled in Z [mm]");
	m_LorentzMap_e->GetZaxis()->SetTitleOffset(1.6);
	m_LorentzMap_e->SetTitle("Electron Lorentz Map");
	m_LorentzMap_e->GetZaxis()->SetTitle("Tangent Lorentz Angle");
	
	m_LorentzMap_h->GetXaxis()->SetNdivisions(505);
    m_LorentzMap_h->GetYaxis()->SetNdivisions(505);
    m_LorentzMap_h->GetXaxis()->SetTitle("Initial Position in Z [mm]");
    m_LorentzMap_h->GetYaxis()->SetTitle("Distance Traveled in Z [mm]");
    m_LorentzMap_h->GetZaxis()->SetTitleOffset(1.6);
    m_LorentzMap_h->SetTitle("Hole Lorentz Map");
    m_LorentzMap_h->GetZaxis()->SetTitle("Tangent Lorentz Angle");

	EFieldMap->GetXaxis()->SetTitle("Depth (z) [#mum]");
	EFieldMap->GetYaxis()->SetTitle("E field [V/cm]");
	EFieldMap->GetYaxis()->SetTitleOffset(1.6);
	EFieldMap->GetYaxis()->SetTitleSize(0.03);
	EFieldMap->GetXaxis()->SetTitleSize(0.03);
}

const std::pair<double,double> AthenaMapGenerator::getMobility(double ElectricField, double Temperature){

  //Returns the electron/hole mobility *in the z direction*
  //Note, this already includes the Hall scattering factor!
  //These parameterizations come from C. Jacoboni et al., Solid-State Electronics 20 89. (1977) 77.  (see also https://cds.cern.ch/record/684187/files/indet-2001-004.pdf).
  //electrons
  double vsat_e = 15.3*pow(Temperature,-0.87);// mm/ns
  double ecrit_e = 1.01E-7*pow(Temperature,1.55);// MV/mm
  double beta_e = 2.57E-2*pow(Temperature,0.66);//dimensionless
  double r_e = 1.13+0.0008*(Temperature-273.);//Hall scaling factor
  //holes
  double vsat_h = 1.62*pow(Temperature,-0.52);// mm/ns
  double ecrit_h = 1.24E-7*pow(Temperature,1.68);// MV/mm
  double beta_h = 0.46*pow(Temperature,0.17);
  double r_h = 0.72 - 0.0005*(Temperature-273.);

  double num_e = vsat_e/ecrit_e;
  double den_e = pow(1+pow((ElectricField/ecrit_e),beta_e),(1/beta_e));
  double mobility_e = r_e*num_e/den_e;

  double num_h = vsat_h/ecrit_h;
  double den_h = pow(1+pow((ElectricField/ecrit_h),beta_h),(1/beta_h));
  double mobility_h =  r_h*num_h/den_h;

  return std::make_pair( mobility_e, mobility_h );
}

double AthenaMapGenerator::getTanLorentzAngle(double ElectricField, double Temperature, double bField,  bool isHole){
    double hallEffect = 1.;//already in mobility//= 1.13 + 0.0008*(temperature - 273.0); //Hall Scattering Factor - taken from https://cds.cern.ch/record/684187/files/indet-2001-004.pdf
    if (isHole) hallEffect = 0.72 - 0.0005*(Temperature - 273.0);
    std::pair<double,double> mobility = getMobility(ElectricField, Temperature);
    double mobility_object = mobility.first;
    if(isHole) mobility_object = mobility.second;
    double tanLorentz = hallEffect*mobility_object*bField*(1.0E-3);  //unit conversion
    return tanLorentz;
}

const std::pair<double,double> AthenaMapGenerator::getTrappingTime(){ 

  m_Fluence *= 1e14;       // neq/cm^2

  double k1_e = 1.7e-16; // cm^2/ns
  double k2_e = 0.11;    // 1/ns 

  double k1_h = 2.8e-16; // cm^2/ns
  double k2_h = 0.09;    // 1/ns

  double beta_e = k1_e + k2_e/m_Fluence;
  double beta_h = k1_h + k2_h/m_Fluence;

  double tau_tr_e = 1.0/(beta_e*m_Fluence);
  double tau_tr_h = 1.0/(beta_h*m_Fluence);

  return std::make_pair( tau_tr_e, tau_tr_h );  
}

void AthenaMapGenerator::generateDistanceTimeMap(TH1F* EFieldMap, double Temperature){

    double bField = 2;//Tesla
    
    //Y-axis is time charge carrier travelled for,
    //X-axis is initial position of charge carrier,
    //Z-axis is final position of charge carrier
    for(int i=1; i<= m_DistanceMap_e->GetNbinsX(); i++){
      for(int j=1; j<= m_DistanceMap_e->GetNbinsY(); j++){
	    m_DistanceMap_h->SetBinContent(i,j,m_SensorThickness_mm); //if you travel long enough, you will reach the electrode.
        m_DistanceMap_e->SetBinContent(i,j,0.);              //if you travel long enough, you will reach the electrode.
      }
    }

    for (int i=1; i<= m_DistanceMap_e->GetNbinsX(); i++){ //Loop over initial position of charge carrier (in z)
	    double time_e = 0.; //ns
	    double time_h = 0.; //ns
	    double distanceTravelled_e=0; //mm 
	    double distanceTravelled_h=0; //mm
	    double drift_e = 0.; //mm
	    double drift_h = 0.; //mm

	    for (int j=i; j >= 1; j--){ //Lower triangle 
		double dz = m_DistanceMap_e->GetXaxis()->GetBinWidth(j); //mm
		double z_j = m_DistanceMap_e->GetXaxis()->GetBinCenter(j); //mm

        double Ez = EFieldMap->GetBinContent(EFieldMap->GetXaxis()->FindBin(z_j*1000))/1e7; // in MV/mm;  
        std::pair<double, double> mu = getMobility(Ez, Temperature); //mm^2/MV*ns
		if (Ez > 0){
	        //Electrons
	        //double tanLorentzAngle = mu.first*bField*(1.0E-3); //rad, unit conversion; pixelPitch_eta-Field is in T = V*s/m^2
            double tanLorentzAngle = getTanLorentzAngle(Ez, Temperature, bField, false);
	        time_e += dz/(mu.first*Ez); //mm * 1/(mm/ns) = ns

	        //Fill: time charge carrier travelled for, given staring position (i) and final position (z_j)
	        m_DistanceMap_e->SetBinContent(i,m_DistanceMap_e->GetYaxis()->FindBin(time_e),z_j);
	        
	        drift_e += dz*tanLorentzAngle; //Increment the total drift parallel to plane of sensor
	        distanceTravelled_e += dz; //mm (travelled in z)
	        m_LorentzMap_e->SetBinContent(i,j,drift_e/distanceTravelled_e); 
		    }
		    m_TimeMap_e->SetBinContent(i,time_e);
	    }
        //Mainly copied from l416 ff changed naming k=>j
        //https://gitlab.cern.ch/radiationDamageDigitization/radDamage_athena_rel22/blob/rel22_radDamageDev_master/scripts/SaveMapsForAthena.C
        for (int j=i; j <= m_DistanceMap_e->GetNbinsX(); j++){ //holes go the opposite direction as electrons.

            double dz = m_DistanceMap_e->GetXaxis()->GetBinWidth(j); //similar to _h
			//double Ez = eFieldMap->GetBinContent(eFieldMap->GetXaxis()->FindBin(z_i*1000))/1e7; // in MV/mm;  
			double z_j= m_DistanceMap_e->GetXaxis()->GetBinCenter(j); //mm //similar to _h
	        double Ez = EFieldMap->GetBinContent(EFieldMap->GetXaxis()->FindBin(z_j*1000))/1e7; // in MV/mm;  
	        std::pair<double, double> mu = getMobility(Ez, Temperature); //mm^2/MV*ns
            if (Ez > 0){
	            //Holes
			    //std::pair<double, double> mu = getMobility(Ez, temperature); //mm^2/MV*ns
                //double tanLorentzAngle = mu.second*bField*(1.0E-3); //rad 
                double tanLorentzAngle = getTanLorentzAngle(Ez, Temperature, bField, true);
                time_h+=dz/(mu.second*Ez); //mm * 1/(mm/ns) = ns  
                m_DistanceMap_h->SetBinContent(i,m_DistanceMap_h->GetYaxis()->FindBin(time_h),z_j);
        
                drift_h+=dz*tanLorentzAngle;
                distanceTravelled_h += dz; //mm 
                m_LorentzMap_h->SetBinContent(i,j,drift_h/distanceTravelled_h);
    		}

			m_TimeMap_h->SetBinContent(i,time_h);
    	}
    }
}

void AthenaMapGenerator::generateAverageLorentzAngleMap(){

    // get trapping constants
    std::pair<double,double> tau_tr = getTrappingTime();
    double tau_tr_e = tau_tr.first;
    double tau_tr_h = tau_tr.second;

    // drift times
    double drifttime_e = 0; // ns
    double drifttime_h = 0; // ns

    // stopping positions
    double zout_e = 0; // mm
    double zout_h = 0; // mm

    // initial position
    double zin = 0; // mm

    // number of simulated trapping times per z position
    int ntrials = 100000; 

    // number of zin positions
    int nzin = m_DistanceMap_e->GetNbinsX();


    // loop over all initial positions
    for (int i = 1; i <= nzin; i++ ) {
      zin = m_DistanceMap_e->GetXaxis()->GetBinCenter(i);
      //std::cout << "zin = " << zin << " mm\n";

      // generate ntrials trapping times
      for (int j = 0; j < ntrials; j++ ) {
        // drift times
        double u = gRandom->Uniform();
        drifttime_e = (-1.) * (tau_tr_e) * logf(u);
        //std::cout << "  drifttime_e = " << drifttime_e << " ns\n";
        u = gRandom->Uniform();
        drifttime_h = (-1.) * (tau_tr_h) * logf(u);
        //std::cout << "  drifttime_h = " << drifttime_h << " ns\n";

        // stopping positions
        zout_e = m_DistanceMap_e->GetBinContent(m_DistanceMap_e->FindBin(zin,drifttime_e));
        //std::cout << "  zout_e = " << zout_e << " mm\n";
        zout_h = m_DistanceMap_h->GetBinContent(m_DistanceMap_h->FindBin(zin,drifttime_h));
        //std::cout << "  zout_h = " << zout_h << " mm\n";

        if ( zout_e < 0 ) { zout_e = 0; }
        if ( zout_h > m_SensorThickness_mm ) { zout_h = m_SensorThickness_mm; }
        //std::cout << "  zout_h = " << zout_h << " mm\n";

        // distance travelled
        //double distance_e = fabs(zout_e-zin);
        //std::cout << "  distance_e = " << distance_e << " mm\n";
        //double distance_h = fabs(zout_h-zin);
        //std::cout << "  distance_h = " << distance_h << " mm\n";

        // Lorentz angle deflection
        double tanLA_e = m_LorentzMap_e->GetBinContent(m_LorentzMap_e->FindBin(zin,zout_e));
        //std::cout << "  tanLA_e = " << tanLA_e << "\n";
        // NB in lorentzMap_h the vertical axis is the final location in z and not the distance travelled
        double tanLA_h = m_LorentzMap_h->GetBinContent(m_LorentzMap_h->FindBin(zin,zout_h));
        //std::cout << "  tanLA_h = " << tanLA_h << "\n\n";

        // fill TProfile of Lorentz angle deflection
        m_AverageLorentzAngle_e->Fill(zin,tanLA_e);
        m_AverageLorentzAngle_h->Fill(zin,tanLA_h);

      }
    }
}

void AthenaMapGenerator::saveGlobalInformation(){

    auto InfoTree = std::make_unique<TTree>("MapInfo", "MapInfo");

    auto TempFluence = m_Fluence*1e-14;

    InfoTree->Branch("temperature", &m_Temperature, "temperature/D");
    InfoTree->Branch("sensorsize", &m_SensorThickness_mum, "sensorsize/D");  
    InfoTree->Branch("fluence", &TempFluence, "fluence/D");  
    InfoTree->Branch("voltage", &m_BiasVoltage, "voltage/D");   

    InfoTree->Fill();
    InfoTree->Write();
}

void AthenaMapGenerator::SaveMapsForAthena(){

	//Thickness is in mm
	//biasVoltage is in volts
	//fluence is in e14 1 MeV neq/cm^2
	//temperature is in celsius
    //doInterpolation: when false copy the TCAD ascii file values instead of interpolating
    //if the corresponding fluence/voltage file is not found, issue a WARNING message and do the interpolation

    using namespace RadDam_Helper;

	std::string RamoFileName;
    if(m_SensorThickness_mum<=200) RamoFileName = "RamoMaps/absRamo3D-map-200um-output.root";
    if(m_SensorThickness_mum>200) RamoFileName = "RamoMaps/test_Blayer.root";

    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Get Ramo Maps." << std::endl;

    TFile* RamoFile = new TFile(RamoFileName.c_str(), "READ");
	TH3F* ramoPotentialMap = (TH3F*)RamoFile->Get("hramomap1");

    //EField Interpolator
    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Inisilaise Interpolator." << std::endl;
    Interpolator* IP = new Interpolator(m_Fluence, m_BiasVoltage, (int)m_SensorThickness_mum, m_ComparisonDirectory);
    auto EFieldMap = (TH1F*)IP->GetEField();

    if(!EFieldMap) std::cout << "INFO\t" << "Efield is null." << std::endl;

    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Generate Distance Time Map." << std::endl;
	generateDistanceTimeMap(EFieldMap, m_Temperature + 273.);

    std::string FileName;
    TString Voltage_String = TString::Format("%.0f",m_BiasVoltage);
    TString Fluence_String = TString::Format("%.2f",m_Fluence);

    std::string Voltage_str(Voltage_String.Data());
    std::string Fluence_str(Fluence_String.Data());

    boost::replace_all(Fluence_str, ".", "_");
    boost::replace_all(Voltage_str, ".", "_");
    
    if(m_SensorThickness_mum<=200) FileName = m_OutputDirectory + "maps_IBL_PL_"+Voltage_str+"_fl"+Fluence_str+".root";
    if(m_SensorThickness_mum>200) FileName = m_OutputDirectory + "maps_PIX_"+Voltage_str+"_fl"+Fluence_str+".root";

    TFile* File = new TFile(FileName.c_str(),"RECREATE");

    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Decorate Histograms." << std::endl;
    DecorateHistograms(EFieldMap);

	m_DistanceMap_e->Write();
	m_DistanceMap_h->Write();
	m_TimeMap_e->Write();
	m_TimeMap_h->Write();
	m_LorentzMap_e->Write();
	m_LorentzMap_h->Write();
	EFieldMap->Write();
	ramoPotentialMap->Write();

    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Generate Lorentz Angle." << std::endl;
    generateAverageLorentzAngleMap();

    m_AverageLorentzAngle_e->Write();
    m_AverageLorentzAngle_h->Write();

    std::cout << "INFO\t" << "AthenaMapGenrator::SaveMapsForAthena: Save General Information." << std::endl;
    saveGlobalInformation();

	File->Write();
    RamoFile->Close();

    delete File;
    delete RamoFile;
    delete IP;
}