#include "RadDam/MapComparison.h"
#include "RadDam/HelperFunctions.h"
#include "AtlasUtils/AtlasStyle.h"
#include "AtlasUtils/AtlasLabels.h"
#include "AtlasUtils/AtlasUtils.h"

#include "TROOT.h"
#include "TH1.h"
#include "TF1.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TVectorD.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TChain.h"
#include <TFile.h>
#include "TLine.h"
#include <TMath.h>
#include <TGraph.h>
#include <algorithm>
#include <stdexcept>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/algorithm/string/replace.hpp>

MapComparison::MapComparison(std::string InputFileName, std::string OutputDirectory){
  m_InputFileName = InputFileName;
  m_OutputDirectory = OutputDirectory;

  SetAtlasStyle();
}

MapComparison::MapComparison(TH1F* EField, double Fluence, double Voltage, int SensorSize, std::string OutputDirectory){
  m_OutputDirectory = OutputDirectory;

  m_MapSensorSize = SensorSize;
  m_MapFluence = Fluence;
  m_MapVoltage = Voltage;

  m_InputEField = EField;

  if(!m_InputEField){
    std::cout << "MapComparison::GetSensorInformation: Cannot read EField from File: " << m_InputFileName << "." <<std::endl;
    exit(EXIT_FAILURE);
  }

  SetAtlasStyle();
}

MapComparison::~MapComparison(){

  for(uint iContainer=0; iContainer<m_EFieldProfiles.size(); iContainer++){
    delete m_EFieldProfiles.at(iContainer);
  }

  delete m_InputEField;
  delete m_InputFile;

}

void MapComparison::GetSensorInformation(){

  TChain* Chain = new TChain("MapInfo");
  Chain->Add(m_InputFileName.c_str());

  if(!Chain){
    std::cout << "MapComparison::GetSensorInformation: Cannot read File: " << m_InputFile << " or Tree 'MapInfo'." <<std::endl;
    exit(EXIT_FAILURE);
  }

  double Temperature = 0;
  double SensorSize = 0;
  double Fluence = 0;
  double Voltage = 0;

  Chain->SetBranchAddress("temperature", &Temperature);
  Chain->SetBranchAddress("sensorsize", &SensorSize);
  Chain->SetBranchAddress("fluence", &Fluence);
  Chain->SetBranchAddress("voltage", &Voltage);

  Chain->GetEntry(0);

  m_MapTemperature = Temperature;
  m_MapSensorSize = (int)SensorSize;
  m_MapFluence = Fluence;
  m_MapVoltage = Voltage; 

  m_InputFile = new TFile(m_InputFileName.c_str(), "READ");
  m_InputEField = (TH1F*)m_InputFile->Get("EField1D");

  if(!m_InputEField){
    std::cout << "MapComparison::GetSensorInformation: Cannot read EField from File: " << m_InputFileName << "." <<std::endl;
    exit(EXIT_FAILURE);
  }
  delete Chain;
}

void MapComparison::InitaliseFiles(){

  using namespace RadDam_Helper;

  if(m_MapSensorSize>200 && m_MapSensorSize<=250){
    m_MapSensorSize = 250;
    m_TCAD_List = "TCAD_Blayer_efields/fei4-250um/";
  }

  if(m_MapSensorSize<=200){
    m_MapSensorSize = 200;
    m_TCAD_List = "TCAD_IBL_efields/fei4-200um/";
  }

  if(m_MapSensorSize>250){
    std::cerr <<"ERROR: MapComparison::InitaliseFiles: Sensor Depth has to be smaller than 250 [µm]." <<std::endl;
    exit(EXIT_FAILURE);    
  }

  auto FluenceDirectories = GetDirectoryList(m_TCAD_List.c_str());

  //Double Loop over all Fl* Directories and E_Field*V.dat Files to get the values and the corresponding voltage and fluence values.
  //Save all information in the EFieldProfiles which are structs EFieldContainers having all information about the profile.

  for(uint iDirectory = 0; iDirectory<FluenceDirectories.size(); iDirectory++){

    //Create Temporary Objects for collecting available Values
    std::pair<double, std::vector<double>> FluenceVoltage_Pair;
    std::vector<double> VoltageValues;

    //Get Fluence Value from the Directory List:
    std::string Fluence_String = FluenceDirectories.at(iDirectory);
    Fluence_String = removeAll(Fluence_String, "fl");
    double FluenceValue = 0;
    try{
      FluenceValue = std::stod(Fluence_String);
    }
    catch(const std::invalid_argument&){
      //If Argument of string to double conversion is invalid, just skip Directory.
      continue;
    }

    auto VoltageFiles = GetDirectoryList((m_TCAD_List + FluenceDirectories.at(iDirectory) + "/reference/").c_str());

    for(uint iFile = 0; iFile<VoltageFiles.size(); iFile++){
      //Get also the Voltage Value

      std::string Voltage_String = VoltageFiles.at(iFile);
      Voltage_String = removeAll(Voltage_String, "E_Field-");
      Voltage_String = removeAll(Voltage_String, "V.dat");

      double VoltageValue = 0;

      try{
        VoltageValue = std::stod(Voltage_String);
      }
      catch(const std::invalid_argument&){
        //If Argument of string to double conversion is invalid, just skip File.
        continue;
      }

      VoltageValues.push_back(VoltageValue);

      std::string InputFilePath = m_TCAD_List  + FluenceDirectories.at(iDirectory) + "/reference/" + VoltageFiles.at(iFile);

      //Function to Fill the Containers and collect them as a vector of pointers which is called EFieldProfiles.
      FillEFieldContainer(InputFilePath.c_str(), FluenceValue, VoltageValue);
    }

    FluenceVoltage_Pair.first = FluenceValue*1e-14;
    FluenceVoltage_Pair.second = VoltageValues;
    m_Available_FluenceValues.push_back(FluenceValue*1e-14);
    m_Available_TCAD_Values.push_back(FluenceVoltage_Pair);
  }
}

void MapComparison::FillEFieldContainer(std::string InputFile, double FluenceValue, double VoltageValue){

  EFieldContainer* EC = new EFieldContainer;
  TH1F* EField = new TH1F("", "", m_MapSensorSize, -0.5, m_MapSensorSize-0.5);

  EC->FluenceValue = FluenceValue*1e-14; //Fluence is in e14 1 MeV neq/cm^2
  EC->VoltageValue = VoltageValue;  // Voltage is in V
  EC->SensorSize = m_MapSensorSize;   // Sensor Depth is in µm

  std::ifstream File;
  File.open(InputFile.c_str());

  if(!File){
    std::cout << "ERROR: Interpolator::FillEFieldContainer:: Could not open File: " << InputFile <<std::endl;
    exit(EXIT_FAILURE);
  }

  double SensorPosition = 0;
  double EFieldValue = 0;

  while(File >> SensorPosition >> EFieldValue){
    EField->Fill(SensorPosition, EFieldValue);
  }

  File.close();

  EC->EField = EField;
  m_EFieldProfiles.push_back(EC);
}

void MapComparison::GetEFieldVariations(){

  using namespace RadDam_Helper;

  m_Fluence_Up = FindClosestElement(m_Available_FluenceValues, m_MapFluence, true);
  m_Fluence_Down = FindClosestElement(m_Available_FluenceValues, m_MapFluence, false);

  for(uint iFluence=0; iFluence<m_Available_TCAD_Values.size(); iFluence++){
    if(m_Fluence_Up == m_Available_TCAD_Values.at(iFluence).first){
      m_Voltage_UpUp = FindClosestElement(m_Available_TCAD_Values.at(iFluence).second, m_MapVoltage, true);
      m_Voltage_UpDown = FindClosestElement(m_Available_TCAD_Values.at(iFluence).second, m_MapVoltage, false);
    }
  }

  for(uint iFluence=0; iFluence<m_Available_TCAD_Values.size(); iFluence++){
    if(m_Fluence_Down == m_Available_TCAD_Values.at(iFluence).first){
      m_Voltage_DownUp = FindClosestElement(m_Available_TCAD_Values.at(iFluence).second, m_MapVoltage, true);
      m_Voltage_DownDown = FindClosestElement(m_Available_TCAD_Values.at(iFluence).second, m_MapVoltage, false);
    }
  }

  for(uint iTCAD=0; iTCAD<m_EFieldProfiles.size(); iTCAD++){

    if(m_EFieldProfiles.at(iTCAD)->FluenceValue == m_Fluence_Up and m_EFieldProfiles.at(iTCAD)->VoltageValue == m_Voltage_UpUp){
      m_EField_FluenceUp_VoltageUp = m_EFieldProfiles.at(iTCAD)->EField;
      ExtrapolateEdges(m_EField_FluenceUp_VoltageUp);
      }
    if(m_EFieldProfiles.at(iTCAD)->FluenceValue == m_Fluence_Up and m_EFieldProfiles.at(iTCAD)->VoltageValue == m_Voltage_UpDown){
      m_EField_FluenceUp_VoltageDown = m_EFieldProfiles.at(iTCAD)->EField;
      ExtrapolateEdges(m_EField_FluenceUp_VoltageDown);
    }
    if(m_EFieldProfiles.at(iTCAD)->FluenceValue == m_Fluence_Down and m_EFieldProfiles.at(iTCAD)->VoltageValue == m_Voltage_DownUp){
      m_EField_FluenceDown_VoltageUp = m_EFieldProfiles.at(iTCAD)->EField;
      ExtrapolateEdges(m_EField_FluenceDown_VoltageUp);
    }
    if(m_EFieldProfiles.at(iTCAD)->FluenceValue == m_Fluence_Down and m_EFieldProfiles.at(iTCAD)->VoltageValue == m_Voltage_DownDown){
      m_EField_FluenceDown_VoltageDown = m_EFieldProfiles.at(iTCAD)->EField;
      ExtrapolateEdges(m_EField_FluenceDown_VoltageDown);
    }
  }

  if(!m_EField_FluenceUp_VoltageUp) m_EField_FluenceUp_VoltageUp = m_InputEField;
  if(!m_EField_FluenceUp_VoltageDown) m_EField_FluenceUp_VoltageDown = m_InputEField;
  if(!m_EField_FluenceDown_VoltageUp) m_EField_FluenceDown_VoltageUp = m_InputEField;
  if(!m_EField_FluenceDown_VoltageDown) m_EField_FluenceDown_VoltageDown = m_InputEField;
}

void MapComparison::PlotEfield(){

  TString Voltage_String = TString::Format("%.0f",m_MapVoltage);
  TString Fluence_String = TString::Format("%.2f",m_MapFluence);

  std::string Voltage_str(Voltage_String.Data());
  std::string Fluence_str(Fluence_String.Data());

  boost::replace_all(Fluence_str, ".", "_");
  boost::replace_all(Voltage_str, ".", "_");

  std::string SensorName = "BLayer";
  if(m_MapSensorSize == 200) SensorName = "IBL";

  std::string SaveName = m_OutputDirectory + "Compraison_Map_" + SensorName + "_fl" + Fluence_str + "_V" + Voltage_str + ".pdf";

  TCanvas c("","",600,700);
  TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.0);
  TPad pad2("pad2","pad2", 0.0, 0.0, 1.0, 0.3);
  pad1.SetBottomMargin(0.02);
  pad1.SetTopMargin(0.04);
  pad1.SetLeftMargin(0.12);
  pad1.SetBorderMode(0);

  pad2.SetTopMargin(0.02);
  pad2.SetBottomMargin(0.275);
  pad2.SetLeftMargin(0.12);
  pad1.Draw();
  pad2.Draw();

  std::vector<double> Maxima;
  Maxima.push_back(m_InputEField->GetMaximum());
  Maxima.push_back(m_EField_FluenceUp_VoltageUp->GetMaximum());
  Maxima.push_back(m_EField_FluenceUp_VoltageDown->GetMaximum());
  Maxima.push_back(m_EField_FluenceDown_VoltageUp->GetMaximum());
  Maxima.push_back(m_EField_FluenceDown_VoltageDown->GetMaximum());

  std::vector<double> Minima;
  Minima.push_back(m_InputEField->GetMinimum());
  Minima.push_back(m_EField_FluenceUp_VoltageUp->GetMinimum());
  Minima.push_back(m_EField_FluenceUp_VoltageDown->GetMinimum());
  Minima.push_back(m_EField_FluenceDown_VoltageUp->GetMinimum());
  Minima.push_back(m_EField_FluenceDown_VoltageDown->GetMinimum());

  m_InputEField->SetMaximum(1.1* *std::max_element(Maxima.begin(), Maxima.end()));
  m_InputEField->SetMinimum(0.9* *std::min_element(Minima.begin(), Minima.end()));

  m_InputEField->SetMarkerSize(0);
  m_InputEField->SetLineColor(kBlue);
  m_InputEField->SetLineStyle(1);

  m_InputEField->GetXaxis()->SetLabelSize(0);

  m_InputEField->GetYaxis()->SetLabelSize(0.03);
  m_InputEField->GetYaxis()->SetLabelFont(42);

  m_InputEField->GetYaxis()->SetTitleFont(42);
  m_InputEField->GetYaxis()->SetTitleSize(0.04);
  m_InputEField->GetYaxis()->SetTitleOffset(1.4);
  m_InputEField->GetYaxis()->SetTitle("E Field [V/cm]");

  m_EField_FluenceUp_VoltageUp->SetMarkerSize(0);
  m_EField_FluenceUp_VoltageUp->SetLineColor(kGreen);
  m_EField_FluenceUp_VoltageUp->SetLineStyle(1);

  m_EField_FluenceUp_VoltageDown->SetMarkerSize(0);
  m_EField_FluenceUp_VoltageDown->SetLineColor(kRed);
  m_EField_FluenceUp_VoltageDown->SetLineStyle(1);

  m_EField_FluenceDown_VoltageUp->SetMarkerSize(0);
  m_EField_FluenceDown_VoltageUp->SetLineColor(kOrange);
  m_EField_FluenceDown_VoltageUp->SetLineStyle(1);

  m_EField_FluenceDown_VoltageDown->SetMarkerSize(0);
  m_EField_FluenceDown_VoltageDown->SetLineColor(kViolet);
  m_EField_FluenceDown_VoltageDown->SetLineStyle(1);

  pad1.cd();

  m_InputEField->Draw("Hist");
  m_EField_FluenceUp_VoltageUp->Draw("Hist same");
  m_EField_FluenceUp_VoltageDown->Draw("Hist same");
  m_EField_FluenceDown_VoltageUp->Draw("Hist same");
  m_EField_FluenceDown_VoltageDown->Draw("Hist same");

  double textHeight = 0.07*(abs(pad1.GetX1()-pad1.GetX2()));
  double legX1 = 1-2.3*0.2*(abs(pad1.GetX1()-pad1.GetX2()));
  double legX2 = 1-0.1*(abs(pad1.GetX1()-pad1.GetX2()));
  double legY = 1-0.08*(abs(c.GetY1()-c.GetY2()));

  TLegend leg(legX1, legY-5.*textHeight, legX2, legY);

  leg.AddEntry(m_InputEField, Form("#Phi=%.2f, V=%.0f (IP)", m_MapFluence, m_MapVoltage), "F");
  if(m_InputEField != m_EField_FluenceUp_VoltageUp) leg.AddEntry(m_EField_FluenceUp_VoltageUp, Form("#Phi=%.2f, V=%.0f", m_Fluence_Up, m_Voltage_UpUp), "f");
  if(m_InputEField != m_EField_FluenceUp_VoltageDown and m_EField_FluenceUp_VoltageUp != m_EField_FluenceUp_VoltageDown and m_EField_FluenceUp_VoltageUp != m_EField_FluenceDown_VoltageUp) leg.AddEntry(m_EField_FluenceUp_VoltageDown, Form("#Phi=%.2f, V=%.0f", m_Fluence_Up, m_Voltage_UpDown), "f");
  if(m_InputEField != m_EField_FluenceDown_VoltageUp and m_EField_FluenceDown_VoltageUp != m_EField_FluenceUp_VoltageUp) leg.AddEntry(m_EField_FluenceDown_VoltageUp, Form("#Phi=%.2f, V=%.0f", m_Fluence_Down, m_Voltage_DownUp), "f");
  if(m_InputEField != m_EField_FluenceDown_VoltageDown and m_EField_FluenceDown_VoltageUp != m_EField_FluenceDown_VoltageDown and m_EField_FluenceDown_VoltageUp != m_EField_FluenceDown_VoltageDown) leg.AddEntry(m_EField_FluenceDown_VoltageDown, Form("#Phi=%.2f, V=%.0f", m_Fluence_Down, m_Voltage_DownDown), "f");

  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.SetTextAlign(32);
  leg.SetMargin(0.);

  leg.Draw();

  pad2.cd();

  //Clone Histos:
  m_Ratio_EField_FluenceUp_VoltageUp = (TH1F*)m_EField_FluenceUp_VoltageUp->Clone();
  m_Ratio_EField_FluenceUp_VoltageDown = (TH1F*)m_EField_FluenceUp_VoltageDown->Clone();
  m_Ratio_EField_FluenceDown_VoltageUp = (TH1F*)m_EField_FluenceDown_VoltageUp->Clone();
  m_Ratio_EField_FluenceDown_VoltageDown = (TH1F*)m_EField_FluenceDown_VoltageDown->Clone();

  m_Ratio_EField_FluenceUp_VoltageUp->Divide(m_InputEField);
  m_Ratio_EField_FluenceUp_VoltageDown->Divide(m_InputEField);
  m_Ratio_EField_FluenceDown_VoltageUp->Divide(m_InputEField);
  m_Ratio_EField_FluenceDown_VoltageDown->Divide(m_InputEField);

  std::vector<double> Ratio_Maxima;
  Ratio_Maxima.push_back(m_Ratio_EField_FluenceUp_VoltageUp->GetMaximum());
  Ratio_Maxima.push_back(m_Ratio_EField_FluenceUp_VoltageDown->GetMaximum());
  Ratio_Maxima.push_back(m_Ratio_EField_FluenceDown_VoltageUp->GetMaximum());
  Ratio_Maxima.push_back(m_Ratio_EField_FluenceDown_VoltageDown->GetMaximum());

  std::vector<double> Ratio_Minima;
  Ratio_Minima.push_back(m_Ratio_EField_FluenceUp_VoltageUp->GetMinimum());
  Ratio_Minima.push_back(m_Ratio_EField_FluenceUp_VoltageDown->GetMinimum());
  Ratio_Minima.push_back(m_Ratio_EField_FluenceDown_VoltageUp->GetMinimum());
  Ratio_Minima.push_back(m_Ratio_EField_FluenceDown_VoltageDown->GetMinimum());

  m_Ratio_EField_FluenceUp_VoltageUp->SetMaximum(1.02* *std::max_element(Ratio_Maxima.begin(), Ratio_Maxima.end()));
  m_Ratio_EField_FluenceUp_VoltageUp->SetMinimum(0.98* *std::min_element(Ratio_Minima.begin(), Ratio_Minima.end()));

  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetLabelSize(0.08);
  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetLabelFont(42);

  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetTitleFont(42);
  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetTitleSize(0.08);
  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetTitleOffset(0.7);
  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->SetTitle("Ratio");
  m_Ratio_EField_FluenceUp_VoltageUp->GetYaxis()->CenterTitle();

  m_Ratio_EField_FluenceUp_VoltageUp->GetXaxis()->SetTitleFont(42);
  m_Ratio_EField_FluenceUp_VoltageUp->GetXaxis()->SetTitleSize(0.1);
  m_Ratio_EField_FluenceUp_VoltageUp->GetXaxis()->SetTitleOffset(1.25);
  m_Ratio_EField_FluenceUp_VoltageUp->GetXaxis()->SetTitle("Sensor Depth [#mum]");

  m_Ratio_EField_FluenceUp_VoltageUp->Draw("Hist");
  m_Ratio_EField_FluenceUp_VoltageDown->Draw("Hist same");
  m_Ratio_EField_FluenceDown_VoltageUp->Draw("Hist same");
  m_Ratio_EField_FluenceDown_VoltageDown->Draw("Hist same");

  const float min = m_InputEField->GetXaxis()->GetBinLowEdge(1);
  const float max = m_InputEField->GetXaxis()->GetBinUpEdge(m_InputEField->GetNbinsX());
  TLine line (min, 1, max, 1);
  line.SetLineColor(kBlack);
  line.SetLineStyle(9);
  line.SetLineWidth(1);
  line.Draw("same");

  c.Print(SaveName.c_str());
}

void MapComparison::ExtrapolateEdges(TH1F* EFieldProfile){

  using namespace RadDam_Helper;

  //Get Content of Histo and generate TGraph to extrapolate the edges.
  std::vector<double> SensorDepth;
  std::vector<double> EField;

  for(int iSensorPart=0; iSensorPart<EFieldProfile->GetNbinsX(); iSensorPart++){
    if(iSensorPart<=3 or iSensorPart>=EFieldProfile->GetNbinsX()-3) continue;

    SensorDepth.push_back((double)iSensorPart);
    EField.push_back(EFieldProfile->GetBinContent(iSensorPart+1));
  }

  TGraph* TempGraph = new TGraph(CastStdVec(SensorDepth), CastStdVec(EField));  

  for(int iSensorPart=0; iSensorPart<EFieldProfile->GetNbinsX(); iSensorPart++){
    if(iSensorPart<=3 or (iSensorPart>=EFieldProfile->GetNbinsX()-3)){
      auto ExtrapolatedEFieldValue = TempGraph->Eval((double)iSensorPart, 0, "S");
      EFieldProfile->SetBinContent(iSensorPart+1, ExtrapolatedEFieldValue);
    }
  }

  delete TempGraph;
}

void MapComparison::CreateComparison(){

  GetSensorInformation();
  InitaliseFiles();
  GetEFieldVariations();
  PlotEfield();

}

void MapComparison::CreateOnFlyComparison(){

  InitaliseFiles();
  GetEFieldVariations();
  PlotEfield();

}
