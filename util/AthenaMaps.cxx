
#include "RadDam/InputOptions.h"
#include "RadDam/AthenaMapGenerator.h"

#include <iostream>

int main(int argc, char *argv[]){

    using namespace RadDam_InputOptions;
    auto RunOptions = get_config_opts(argc, argv);

    for(uint iInputValue=0; iInputValue<RunOptions.SensorThicknesses.size(); iInputValue++){
        AthenaMapGenerator* AMG = new AthenaMapGenerator(RunOptions.SensorThicknesses.at(iInputValue), RunOptions.Voltages.at(iInputValue), RunOptions.Fluences.at(iInputValue), RunOptions.Temperatures.at(iInputValue), RunOptions.OutputDirectory, RunOptions.ComparisonDirectory);
        AMG->SaveMapsForAthena();
        delete AMG;

        std::cout << "INFO\t" << "Map saved." << std::endl;
    }


    return EXIT_SUCCESS;
}