#include "RadDam/InputOptions.h"
#include "RadDam/MapComparison.h"

int main(int argc, char *argv[]){

    if(argc != 3){
        std::cout << "You need to pass two Arguments: the Input Directory with the Maps and a Directory where the Plots are saved." <<std::endl;
        exit(EXIT_FAILURE);
    }

    using namespace RadDam_InputOptions;
    auto InputMaps = GetDirectoryList(argv[1]);
    std::string OutputDirectory = argv[2];

    for(uint iInputMap=0; iInputMap<InputMaps.size(); iInputMap++){
        std::cout << InputMaps.at(iInputMap) << " InputMap." <<std::endl;
        MapComparison* MC = new MapComparison(InputMaps.at(iInputMap), OutputDirectory);
        MC->CreateComparison();
        delete MC;
    }

    return EXIT_SUCCESS;
}