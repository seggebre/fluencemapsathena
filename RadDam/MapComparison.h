#include "TH1.h"
#include <TFile.h>
#include <iostream>
#include <fstream>

class MapComparison{

    public:
        MapComparison(std::string InputFile, std::string OutputDirectory);
        MapComparison(TH1F* EField, double Fluence, double Voltage, int SensorSize, std::string OutputDirectory);
        ~MapComparison();
        void CreateComparison();
        void CreateOnFlyComparison();

    private:
        std::string m_InputFileName;
        std::string m_OutputDirectory;
        TFile* m_InputFile = nullptr;
        TH1F* m_InputEField = nullptr;

        double m_MapFluence;
        double m_MapVoltage;
        double m_MapTemperature;
        int m_MapSensorSize;

        TH1F* m_EField = nullptr;

        TH1F* m_EField_FluenceUp_VoltageUp = nullptr;
        TH1F* m_EField_FluenceUp_VoltageDown = nullptr;
        TH1F* m_EField_FluenceDown_VoltageUp = nullptr;
        TH1F* m_EField_FluenceDown_VoltageDown = nullptr;

        TH1F* m_Ratio_EField_FluenceUp_VoltageUp = nullptr;
        TH1F* m_Ratio_EField_FluenceUp_VoltageDown = nullptr;
        TH1F* m_Ratio_EField_FluenceDown_VoltageUp = nullptr;
        TH1F* m_Ratio_EField_FluenceDown_VoltageDown = nullptr;

        double m_Fluence_Up;
        double m_Fluence_Down;

        double m_Voltage_UpUp;
        double m_Voltage_UpDown;
        double m_Voltage_DownUp;
        double m_Voltage_DownDown;

        struct EFieldContainer{
            double FluenceValue;
            double VoltageValue;
            int SensorSize;
            TH1F* EField = nullptr;
        }; 

        std::vector<std::pair<double, std::vector<double>>> m_Available_TCAD_Values;
        std::vector<double> m_Available_FluenceValues;

        std::vector<EFieldContainer*> m_EFieldProfiles;
        std::string m_TCAD_List;

        void GetSensorInformation();
        void InitaliseFiles();
        void FillEFieldContainer(std::string InputFile, double FluenceValue, double VoltageValue);
        void ExtrapolateEdges(TH1F* EFieldProfile);
        void PlotEfield();     
        void GetEFieldVariations();
};