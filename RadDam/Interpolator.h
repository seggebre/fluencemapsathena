#include "TH1.h"
#include <TFile.h>
#include <iostream>
#include <fstream>

class Interpolator{

    public:

        Interpolator(double aimFluence, double aimVoltage, int SensorDepth, std::string ComparisonDirectory);
        ~Interpolator();

        TH1F* GetEField();
        TH1F* m_EFieldProfile = nullptr;

    private:

        struct EFieldContainer{
            double FluenceValue;
            double VoltageValue;
            int SensorSize;
            TH1F* EField;
        };       

        std::vector<EFieldContainer*> m_EFieldProfiles; 

        TFile* m_InputFile = nullptr;
        std::string m_FileName;
        std::string m_TCAD_List;

        std::vector<double> m_PossibleFluenceValues;
        std::vector<double> m_PossibleVoltageValues;

        double m_aimFluence = 0;
        double m_aimVoltage = 0;
        int m_SensorDepth = 0;
        std::string m_ComparisonDirectory;

        std::string m_TextFluence;
        std::string m_TextVoltage;
        std::string m_TextSensorDepth;

        TH1F* CreateEFieldProfile();
        void InitaliseFiles();
        void FillEFieldContainer(std::string InputFile, double FluenceValue, double VoltageValue);
        double EstimateEField(int SensorPart, std::string InterpolationType);
        void ExtrapolateEdges();
        void ScaleEField();
};