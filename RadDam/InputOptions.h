#include <string>
#include <vector>

#pragma once

namespace RadDam_InputOptions{

    // Structure to store the information of the config file
    struct ConfigOptions{
        std::vector<double> SensorThicknesses;
        std::vector<double> Voltages;
        std::vector<double> Fluences;
        std::vector<double> Temperatures;
        std::string OutputDirectory;
        std::string ComparisonDirectory;
    };

    // Functions for reading directories and files
    void PrintUsage();
    std::vector<std::string> GetFileContent(std::string FileName);
    std::vector<std::string> GetDirectoryList(std::string InputPath);
    std::vector<std::string> SplitInput(std::string& s, char delimiter);

    // Parse the command line options
    ConfigOptions get_config_opts(int argc, char* argv[]);


} // namespace ttHML_InputOptions

