#include <iostream>
#include "TH2F.h"
#include "TH1F.h"

class AthenaMapGenerator{

    public:
        AthenaMapGenerator(double SensorThickness, double BiasVoltage, double Fluence, double Temperature, std::string OutputDirectory, std::string ComparisonDirectory);
        ~AthenaMapGenerator();
        void SaveMapsForAthena();

    private:
        double m_SensorThickness_mm;
        double m_SensorThickness_mum;
        double m_BiasVoltage;
        double m_Fluence;
        double m_Temperature;
        std::string m_OutputDirectory;
        std::string m_ComparisonDirectory;

        TH2F* m_DistanceMap_e;
        TH2F* m_DistanceMap_h;
        TH1F* m_TimeMap_e;
        TH1F* m_TimeMap_h;
        TH2F* m_LorentzMap_e;
        TH2F* m_LorentzMap_h;

        TProfile* m_AverageLorentzAngle_e;
        TProfile* m_AverageLorentzAngle_h;

        void WelcomeMessage();
        void DecorateHistograms(TH1F* EFieldMap);
        const std::pair<double,double> getMobility(double ElectricField, double Temperature);
        double getTanLorentzAngle(double ElectricField, double Temperature, double bField,  bool isHole);
        const std::pair<double,double> getTrappingTime();
        void generateDistanceTimeMap(TH1F* EFieldMap, double Temperature);
        void generateAverageLorentzAngleMap();
        void saveGlobalInformation();
};