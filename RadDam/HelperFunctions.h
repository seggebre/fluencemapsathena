#include <string>
#include <vector>
#include "TVectorD.h"

#pragma once

namespace RadDam_Helper{

    // Functions for reading directories and files
    std::vector<std::string> GetDirectoryList(std::string InputPath);
    std::string removeAll(std::string str, const std::string& from);
    TVectorD CastStdVec(const std::vector<double> Input);
    double FindClosestElement(std::vector<double> Vector, double Value, bool Larger);
}