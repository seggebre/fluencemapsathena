# Set the minimum CMake version required to build the project.
cmake_minimum_required( VERSION 3.1 )

# Silence some warnings on macOS with new CMake versions.
if( NOT ${CMAKE_VERSION} VERSION_LESS 3.9 )
   cmake_policy( SET CMP0068 NEW )
endif()

# Set the project's name and version.
project( RadDam )

# Set up the "C++ version" to use.
set( CMAKE_CXX_STANDARD_REQUIRED 11 CACHE STRING
   "Minimum C++ standard required for the build" )
set( CMAKE_CXX_STANDARD 17 CACHE STRING
   "C++ standard to use for the build" )
set( CMAKE_CXX_EXTENSIONS FALSE CACHE BOOL
   "(Dis)allow using compiler extensions" )

# Specify the install locations for libraries and binaries.
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
set( CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib )  # Needed by ROOT_GENERATE_DICTIONARY()

# Set the warning flag(s) to use.
set( CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -pedantic -O3 -g" )

# Add ROOT system directory and require ROOT.
find_package( ROOT 6.26.04 REQUIRED COMPONENTS Core MathCore Graf Hist RIO Tree Gpad Minuit )

set( _atlas_utils_headers AtlasUtils/AtlasLabels.h AtlasUtils/AtlasStyle.h AtlasUtils/AtlasUtils.h )
set( _atlas_utils_sources AtlasUtils/AtlasLabels.C AtlasUtils/AtlasStyle.C AtlasUtils/AtlasUtils.C )
add_library( AtlasUtils SHARED ${_atlas_utils_headers} ${_atlas_utils_sources} )
target_include_directories( AtlasUtils
   PUBLIC ${ROOT_INCLUDE_DIRS}
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}> $<INSTALL_INTERFACE:> )
target_link_libraries( AtlasUtils ${ROOT_LIBRARIES} )

# Public header files for the shared/static library.
set( lib_headers
  RadDam/InputOptions.h
  RadDam/HelperFunctions.h
  RadDam/AthenaMapGenerator.h
  RadDam/Interpolator.h 
  RadDam/MapComparison.h )

set( lib_sources
  Root/InputOptions.cxx
  Root/HelperFunctions.cxx
  Root/AthenaMapGenerator.cxx
  Root/Interpolator.cxx 
  Root/MapComparison.cxx )

# Specify thread options
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads)

# Build the shared library.
add_library( RadDam SHARED ${lib_headers} ${lib_sources} )
target_include_directories( RadDam
   PUBLIC ${ROOT_INCLUDE_DIRS}
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}> $<INSTALL_INTERFACE:> )
target_link_libraries( RadDam ${ROOT_LIBRARIES} Threads::Threads AtlasUtils)
set_property( TARGET RadDam
   PROPERTY PUBLIC_HEADER ${lib_headers} )
target_include_directories(RadDam PUBLIC ${CMAKE_CURRENT_LIST_DIR} )

# Helper macro for building the project's executables.
macro( RadDam_add_executable name )
  add_executable( ${name} ${ARGN} )
  target_include_directories( ${name} PUBLIC ${ROOT_INCLUDE_DIRS} )
  target_link_libraries( ${name} RadDam ${ROOT_LIBRARIES} Threads::Threads AtlasUtils)
  install( TARGETS ${name}
      EXPORT RadDam
    RUNTIME DESTINATION bin )
endmacro( RadDam_add_executable )

RadDam_add_executable( AthenaMaps util/AthenaMaps.cxx )
RadDam_add_executable( CompareAthenaMaps util/CompareAthenaMaps.cxx )